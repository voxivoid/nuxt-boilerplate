module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
  },
  parserOptions: {
    parser: "babel-eslint",
  },
  // required to lint *.vue files
  plugins: [
    "vue",
  ],
  settings: {
    "import/core-modules": ["vue", "vuex"],
    "import/resolver": {
      webpack: {
        config: {
          resolve: {
            alias: {
              "#": __dirname,
              "#api": `${__dirname}/mixins/api`,
            },
          },
        },
      },
    },
  },
  extends: [
    // https://github.com/vuejs/eslint-plugin-vue#priority-a-essential-error-prevention
    // consider switching to `plugin:vue/strongly-recommended` or `plugin:vue/recommended` for stricter rules.
    "plugin:vue/recommended",
    "airbnb-base",
  ],
  // add your custom rules here
  rules: {
    "arrow-parens": ["error", "as-needed"],
    "brace-style": ["error", "stroustrup"],
    "linebreak-style": "off",
    "max-len": "off",
    "no-await-in-loop": "off",
    "no-empty-function": "warn",
    "no-empty": "warn",
    "no-multi-assign": "off",
    "no-nested-ternary": "off",
    "no-param-reassign": "off",
    "no-plusplus": "off",
    "no-return-assign": "off",
    "no-sequences": "off",
    "no-shadow": "off",
    "no-underscore-dangle": "off",
    "no-unused-vars": "warn",
    "no-use-before-define": "off",
    "object-curly-newline": ["error", { multiline: true, consistent: true }],
    "prefer-destructuring": ["warn", { object: true, array: false }],
    quotes: ["error", "double"],
    radix: "off",
  },
};
