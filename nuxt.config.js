require("dotenv-safe").load();

module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: "nuxt-boilerplate",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "Nuxt.js project" },
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      { rel: "stylesheet", href: "https://fonts.googleapis.com/css?family=Roboto" },
      { rel: "stylesheet", href: "https://use.fontawesome.com/releases/v5.0.6/css/all.css" },
    ],
  },
  css: [
    "./node_modules/minireset.css/minireset.css",
    "./node_modules/@makay/flexbox/flexbox.css",
  ],
  /*
  ** Customize the progress bar color
  */
  loading: { color: "#3B8070" },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend(config, { isDev }) {
      if (isDev) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          loader: "eslint-loader",
          exclude: /(node_modules)/,
          options: { fix: true },
        });
      }

      config.resolve.alias["#"] = __dirname;
      config.resolve.alias["#api"] = `${__dirname}/mixins/api`;
    },

    vendor: [
      "lodash",
    ],
  },

  plugins: [],

  modules: [
    "@nuxtjs/axios",
    "@nuxtjs/dotenv",
  ],

  axios: {
    proxy: true,
  },

  proxy: {
    "/api": {
      target: process.env.REST_API_URL,
      pathRewrite: { "^/api": "" },
      // onProxyReq(proxyReq, req) {
      //   console.log(req);
      // },
    },
  },
};
