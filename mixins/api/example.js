export default {
  beforeCreate() {
    this.api = {
      ...this.api,
      example: {
        getExample: () => this.$axios.get("/example").then(response => response.data),
      },
    };
  },
};
